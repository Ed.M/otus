import sys
import logging
import logging.handlers
import os
import errno


LOGGING_LEVEL = 'DEBUG'


name_dir = 'logs'
path = os.path.join(os.path.dirname(os.path.abspath(__file__)), name_dir)

try:
    os.makedirs(path) 
except OSError as exception:
    if exception.errno != errno.EEXIST:
        raise


path = os.path.join(path, 'logs.log')
formatter = logging.Formatter('[%(asctime)s] %(levelname)s %(message)s')

steam = logging.StreamHandler(sys.stderr)
steam.setFormatter(formatter)
steam.setLevel(logging.DEBUG)
log_file = logging.FileHandler(path, encoding='utf8')
log_file.setFormatter(formatter)

logger = logging.getLogger('analyzer_log')
logger.setLevel(logging.DEBUG)
logger.addHandler(steam)
logger.addHandler(log_file)

if __name__ == '__main__':
    try:
        1/0
    except  Exception:
        logger.error('Test error ivent', exc_info=True)
    logger.info('Test info ivent')
    logger.error('Test info eror')
