pylint
autopep8
ipython
pydocstyleastroid==1.6.6
autopep8==1.5
backports.functools-lru-cache==1.6.1
backports.shutil-get-terminal-size==1.0.0
configparser==4.0.2
decorator==4.4.1
enum34==1.1.9
futures==3.3.0
ipython==5.9.0
ipython-genutils==0.2.0
isort==4.3.21
lazy-object-proxy==1.4.3
mccabe==0.6.1
pathlib2==2.3.5
pexpect==4.8.0
pickleshare==0.7.5
prompt-toolkit==1.0.18
ptyprocess==0.6.0
pycodestyle==2.5.0
pydocstyle==3.0.0
Pygments==2.5.2
pylint==1.9.5
scandir==1.10.0
simplegeneric==0.8.1
singledispatch==3.4.0.3
six==1.14.0
snowballstemmer==2.0.0
traitlets==4.3.3
wcwidth==0.1.8
wrapt==1.12.0
