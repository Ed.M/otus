#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import sys
import os
from datetime import datetime
sys.path.append(os.path.join(os.getcwd(), ''))
from log_analyzer import date_parse, process_line, median_array, create_report_name


class TestClass(unittest.TestCase):
    time_str = '20200305'
    line = '1.126.153.80 -  - [29/Jun/2017:04:12:43 +0300] "GET /api/v2/banner/22480465 HTTP/1.1" 200 933 "-" "-" "-" "1498698762-48424485-4707-9839082" "1835ae0f17f" 0.885'
    arr_one, arr_two = [3, 2, 1], [1, 2, 3, 4] 
    date_ = datetime(2020, 3, 5, 0, 0)

    def test_def_date_parse(self):
        test = date_parse(self.time_str)
        self.assertEqual(test, datetime(2020, 3, 5, 0, 0))

    def test_def_process_line(self):
        (url, request_time) = process_line(self.line)
        self.assertEqual((url, request_time), ('/api/v2/banner/22480465', 0.885))

    def test_def_median_array(self):
        median_one = median_array(self.arr_one)
        median_two = median_array(self.arr_two)
        self.assertEqual(median_one, 2)
        self.assertEqual(median_two, 2.5)

    def test_def_create_report_name(self):
        report_name = create_report_name(self.date_)
        self.assertEqual(report_name, 'report-2020.03.05.html')


if __name__ == '__main__':
    unittest.main()
