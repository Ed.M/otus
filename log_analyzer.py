#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gzip
import os
import io
import re
import argparse
import sys
import json
import logging
import errno
from logging_analyzer import logging_setting
from datetime import datetime
from collections import namedtuple, defaultdict
from itertools import ifilter, chain
from string import Template
from decimal import Decimal
from common.variables import *

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


logger = logging.getLogger('analyzer_log')


def xreadlines(log_path):
    '''Функция герератор открывает лог файл с дирректрией log_path и итерируется по строкам.'''
    log = gzip.open(log_path, 'rb') if log_path.endswith(".gz") else open(log_path)

    total = processed = 0
    for line in log:
        parsed_line = process_line(line.decode(ENCODING))
        total += 1
        if parsed_line:
            processed += 1
            yield parsed_line
    percent_pars_line = round(Decimal(processed)/Decimal(total)*100, 2)
    logger.info('%s из %s успешного парсинга. Доля успешного парсинга: %s '.decode(ENCODING) % (processed, total, percent_pars_line))
    if percent_pars_line < CRITICAL_PERCENT:
        logger.info('Низкий процетн успешного парсинга строк: %s. Необходимо сменить шаблон'.decode(ENCODING) % (percent_pars_line))
    log.close()


def process_line(line):
    '''Функция возвращает распаршенные значения строки line: $request $request_time.'''
    regex = r'\s\/\S+'
    spam = re.search(regex, line)
    if not spam:
        return
    request_time = line.split()[-1]
    url, request_time = spam.group(0).strip(), float(request_time)
    return url, request_time


def date_parse(date_str):
    pattern = "%Y%m%d"
    try:
        date_log = datetime.strptime(date_str, pattern)
        return date_log
    except Exception:
        logger.error('Ошибка парсинга даты: Такой даты не существует %s '.decode(ENCODING)
                     % (date_str), exc_info=True)


def get_info_actual_log_file(config):
    '''Функция возвращает именованный кортеж LogInfoFile содержащий информацию о лог файле'''
    try:
        list_dir = os.listdir('.')

    except OSError:
        print OSError

    min_day = 0
    LogInfoFile = namedtuple('LogInfoFile', 'path name datetime report_name')
    LogInfoFile.name = ''

    regex = r'\d{8}(.gz$|$)'
    for file_name in ifilter(lambda x: os.path.isfile(x), list_dir):
        spam = re.search(regex, file_name)
        if spam:
            date_str = spam.group(0).replace('.gz', '')
            date_log = date_parse(date_str)
            if not date_log:
                continue
            if min_day < date_log.toordinal():
                min_day = date_log.toordinal()
                LogInfoFile.path = os.path.join(os.path.abspath('.'), file_name)
                LogInfoFile.name = file_name
                LogInfoFile.datetime = date_log
                LogInfoFile.report_name = create_report_name(date_log)
    if LogInfoFile.name:
        logger.info('Найден файл лога %s'.decode(ENCODING) % (LogInfoFile.name))
        return LogInfoFile
    
    logger.info('В данной директории лог файлов не обнаружено'.decode(ENCODING))
    return LogInfoFile


def get_dictionary_url(LogInfoFile):
    '''Принимает путь к файлу лога log_path, возвращает словарь, где ключи url
    значения список $request_time. Также возвращает total_request_time, 
    total_request_count.'''

    dict_url_log = defaultdict(list)
    total_request_time = total_request_count = 0
    for url, request_time in xreadlines(LogInfoFile.path):
        dict_url_log[url].append(request_time)
        total_request_time += request_time
        total_request_count += 1

    dict_url_log['total_request_time'] = total_request_time
    dict_url_log['total_request_count'] = total_request_count
    return dict_url_log


def median_array(arr):
    '''Возвращает медианное значение массива arr'''
    arr = sorted(arr)
    pos = len(arr) // 2
    return Decimal(arr[pos-1] + arr[pos])/2 if len(arr) % 2 == 0 else arr[pos]


def xcreate_line_table(dict_url_log, config):
    '''Функция генератор возвращает словарь со статистическими данными:
    count - сколько раз встречается URL;
    time_avg -средний $request_time для данного URL'а;
    time_max - аксимальный $request_time для данного URL'а;
    time_sum - cуммарный $request_time для данного URL'а;
    time_med -медиана $request_time для данного URL'а;
    time_perc -суммарный $request_time для данного URL'а, в процентах относительно 
    общего $request_time всехзапросов;
    count_perc - колько раз встречается URL, в процентнах относительно общего числа запросов.
    Останавливает итерирования по достижении итераций = config['REPORT_SIZE'] итераций.
    '''

    total_request_time = Decimal(dict_url_log.pop('total_request_time'))
    total_request_count = Decimal(dict_url_log.pop('total_request_count'))

    NDIGITS = 3
    count_report = 0 
    for url, time_list in sorted(dict_url_log.items(), key=lambda x: sum(x[1]), reverse=True):
        count = len(time_list)
        time_avg = round(Decimal(sum(time_list)) / count, NDIGITS)
        time_max = max(time_list)
        time_sum = round(sum(time_list), NDIGITS)
        time_med = round(median_array(time_list), NDIGITS)
        time_perc = round(Decimal(time_sum) / total_request_time * 100, NDIGITS)
        count_perc = round(Decimal(count) / total_request_count * 100, NDIGITS)


        if count_report == config[REPORT_SIZE]:
            raise StopIteration

        count_report += 1
        yield {
            "count": count, "time_avg": time_avg, "time_max": time_max,
            "time_sum": time_sum, "url": url.encode('utf-8'), "time_med": time_med,
            "time_perc": time_perc, "count_perc": count_perc,
        }


def arg_parser():
    '''Функция осуществляет паринг аргументов -c(--config) path_config_file,
    path_config_file путь до файла конфигурации, в случае без указания аргумента
    path_config_file, возвращает DEFAULT_PATH_CONFIG, иначе None.'''

    DEFAULT_PATH_CONFIG = './config/config.json'
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', default='not_arg', type=str, nargs='?',
                        help="")

    namespace = parser.parse_args(sys.argv[1:])
    path_config = namespace.config

    path_config = path_config or DEFAULT_PATH_CONFIG
    if path_config == 'not_arg':
        return
    return path_config


def read_config_file(conf_path):
    '''Функция читает файл конфигурацуии в формате .json располженный conf_path,
     в случае успешного чтения и налиии ключа 'config' возвращает словарь с
     конфигурационными данными, иначе возвращает {}.
     '''

    conf = open(conf_path, 'rb')
    try:
        conf_read_json = json.load(conf)
        logger.info('Чтение файла конфигурации %s '.decode(ENCODING) % (conf_path))
    except:
        logger.error('Ошибка чтения файла конфигурации %s '.decode(ENCODING) % (conf_path))
        return {}
    conf.close()

    try:
        return conf_read_json['config']
    except:
        logger.error('Файл не содержит валидной конфигурации %s '.decode(ENCODING) % (conf_path))
        return {}


def get_config():
    '''Возвращает словарь кофигурации config, образованный слиянием исходного
    config и загруженного из файла конфигурации (высший приоритет). По умолчанию 
    возвращает исходный config
    '''
    config = {
        REPORT_SIZE: 1000,
        REPORT_DIR: "./reports",
        LOG_DIR: "./log",
    }

    path_config = arg_parser()

    if path_config:
        read_config = read_config_file(path_config)

        if not set(config.keys()) & set(read_config.keys()):
            logger.error('В загруженном конфигурационном файле отсутствуют валидные ключи'.decode(ENCODING))
            return

        for key, value in read_config.items():
            if not key in config.keys():
                continue
            elif config[key] != value:
                logger.info('Измениение конфигурации: %s=%s изменен на %s=%s '.decode(ENCODING) % (
                            key, config[key], key, value))
                config[key] = value
    return config


def report_create(table, LogInfoFile, config):
    '''Функция загружает шаблон отчета report.html, заменяет $table_json
    в report.html на table, и сохраняет отчет.'''
    d = {'table_json': table}

    name_dir = config[REPORT_DIR]
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), name_dir)

    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

    report = open('report.html', 'rb')
    templ = Template(report.read())
    templ = templ.safe_substitute(d)
    report.close()

    ful_path = os.path.join(path, LogInfoFile.report_name)
    save_report(ful_path, templ)
    return


def save_report(ful_path, templ):
    '''Функция сохраняет шаблон templ в формате .html'''
    ff = open(ful_path, 'w')
    ff.write(templ)
    ff.close


def get_table(dict_url_log, config):
    '''Функция возвращает список с данными для таблицы шаблона.'''
    return list(chain([], xcreate_line_table(dict_url_log, config)))


def create_report_name(date):
    pattern = "report-%Y.%m.%d.html"
    return date.strftime(pattern)


def is_report_file(LogInfoFile, config):
    '''Функция проверяет наличие отчета в директории REPORT_DIR и возвращает True,
     если файл существует, иначе False'''
    name_dir = config[REPORT_DIR]
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), name_dir)
    if not os.path.exists(path):
        logger.info('Создана новая дирректория: %s'.decode(ENCODING) % (name_dir))
        return False

    if LogInfoFile.report_name in os.listdir(path):
        logger.info('Отчет %s существует'.decode(ENCODING) % (LogInfoFile.report_name))
        return True
    return False


def main():
    config = get_config()
    if not config:
        return

    LogInfoFile = get_info_actual_log_file(config)
    if not LogInfoFile.name:
        return

    if not is_report_file(LogInfoFile, config):
        dict_url_log = get_dictionary_url(LogInfoFile)
        report_create(get_table(dict_url_log, config), LogInfoFile, config)
    return


if __name__ == "__main__":
    main()
